# Beginner Level Interview

- Pull the git to local environtment
- Set up config and database
- Add new table called students.
- Populate the table column with student column contain id, name, course.
- Create simple model,controller for student.
- Create function that linked to view 
- In view make form that required user to fill up name and student course (both text field).
- Create model to store student data to database.
- Create controller to store the student to database.
- Use the form to store student data.
- Export sql table
- Put sql in git
- Push your code to the gitlab with your name as branch and your level


# Intermediate Level Interview

- Pull the git to local environtment
- Set up config and database
- Add new table called students.
- Populate the table column with student column contain id, name, course.
- Create simple model,controller for student.
- Create function that linked to view 
- In view make form that required user to fill up name and student course (both text field).
- Create model to store student data to database.
- Create controller to store the student to database.
- Use the form to store student data.
- Use library datatable to be able to list student data.
- Create action button in datatable to delete and edit student.
- Complete delete and edit function for student.
- Export sql table
- Push your code to the gitlab with your name as branch and your level


# Advance Level Interview

- Pull the git to local environtment
- Set up config and database
- Add new table called students.
- Populate the table column with student column contain id, name, course.
- Create simple model,controller for student.
- Create function that linked to view 
- In view make form that required user to fill up name and student course (both text field).
- Create model to store student data to database.
- Create controller to store the student to database.
- Use the form to store student data.
- Use library datatable to be able to list student data.
- Create action button in datatable to delete and edit student.
- Complete delete and edit function for student.
- Create crud api with simple api key for student.
- Create postman documentation for the api.
- Provide postman documentation in git folder.
- Push your code to the gitlab with your name as branch and your level



# Sample of branch
-adib-beginner
